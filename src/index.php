<!DOCTYPE html>
<html>
<head>
    <title>Landing Page</title>
    <style>
        /* Add your custom CSS styles here */
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: black;
            color: white;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
            text-align: center;
        }
        .container {
            max-width: 800px;
            padding: 20px;
        }
        h1 {
            color: white;
        }
        .button {
            display: inline-block;
            background-color: #4CAF50;
            color: #fff;
            padding: 10px 20px;
            text-decoration: none;
            border-radius: 4px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Hello , Welcome to Cloud HM!</h1>
        <p>This is a simple landing page created using PHP.</p>
        <p>You can customize the content of this page according to your needs.</p>
        <p>For example, you can add a form below:</p>
    </div>
</body>
</html>
